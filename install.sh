#!/bin/bash

set -e

mv *.ttf /usr/share/fonts/

fc-cache -vfs
